#FizzBuzz
Simple implementation of FizzBuzz

#Dependencies
Only Jest for testing

#Assignment
FizzBuzz test – prosím nevymýšlet příliš komplikované řešení, nejjednodušší, co Vás napadne:

Napište funkci, která vypíše čísla od 1 do 100, ale pro následující podmínky vypíše text:

1. čísla dělitelná 3-mi a 5-ti vypíšou text ‘FizzBuzz’

2. čísla dělitelná 3-mi vypíšou text ‘Fizz’

3. čísla dělitelná 5-ti vypíšou text ‘Buzz’

4. čísla, která nesplňují žádnou z podmínek vypíšou pouze číslo


#Scripts

## Start
Runs the FizzBuzz in default Node.js and prints the FizzBuzz to console:
```
npm run start
yarn start
```

## test
Runs the Jest tests for FizzBuzz:
```
npm run test
yarn test
```
